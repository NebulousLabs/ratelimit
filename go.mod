module gitlab.com/NebulousLabs/ratelimit

go 1.13

require (
	gitlab.com/NebulousLabs/fastrand v0.0.0-20181126182046-603482d69e40
	gitlab.com/NebulousLabs/log v0.0.0-20200604091839-0ba4a941cdc2
	gitlab.com/NebulousLabs/siamux v0.0.0-20200723083235-f2c35a421446
)
